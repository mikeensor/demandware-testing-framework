<?xml version="1.0" encoding="UTF-8"?>
<document xmlns="http://maven.apache.org/XDOC/2.0"
          xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
          xsi:schemaLocation="http://maven.apache.org/XDOC/2.0 http://maven.apache.org/xsd/xdoc-2.0.xsd">

    <properties>
        <title>Demandware Automated Testing Framework</title>
        <author email="mike.ensor@acquitygroup.com">Mike Ensor</author>
    </properties>

    <!-- Optional HEAD element, which is copied as is into the XHTML <head> element -->
    <head>
        <meta content="Documentation site for Automated Testing Framework" name="description"/>
        <title>${title}</title>

        #include("header.vm")
    </head>

    <body>
        <a name="back-to-top" id="back-to-top"></a>
        <h1>Demandware Automated Testing Framework</h1>
        <p>The purpose of this framework is to minimize the amount of bolierplate code required to test Demandware Site
            Genesis 2.0 implementations. The goal of the
            framework is to establish a base level of testing flows for Site Genesis 2.0, provide re-usable and
            extensible
            <em>Page/Module</em>
            objects and the mechanisms to
            extend the framework for deviations from Site Genesis 2.0
        </p>
        <a href="#back-to-top" class="back-to-top">
            <em>Back To Top</em>
        </a>

        <section name="Overview">
            <p>This documentation will cover what and why specific technology has been implemented and how to use the
                framework to add automated testing to a Demandware project
            </p>
            <subsection name="Technologies">
                <ul>
                    <li>Spock - Spock is a BDD Specification testing framework that works with JUnit. Spock's
                        Specifications are the base of this framework's test cases.
                        <a href="https://spock-framework.readthedocs.org/en/latest/">Spock Documentation</a>
                    </li>
                    <li>Geb - Geb is a groovy based abstraction of WebDriver with a focus on simplistic syntax and easy
                        encapsulations.
                        <a href="http://www.gebish.org/manual/current/all.html">Geb Documentation</a>.
                    </li>
                    <li>WebDriver - WebDriver is the successor to Selenium RC. WebDriver utilizes small applications to
                        "Drive" a browser instead of attempting to control the mouse.
                    </li>
                    <li>Maven 3 - Maven is the leading Java based build tool. Maven simplifies dependency management and
                        enforces a consistent project structure
                    </li>
                    <li>Bamboo - Bamboo is a Continuous Integration environment from Atlassian. If you do not have the
                        means to build an infrastructure, check out On-Demand for an
                        in-cloud based CI environment.
                        <a href="http://www.atlassian.com/software/bamboo/overview">Bamboo OnDemand Homepage</a>
                    </li>
                    <li>SauceLabs - SauceLabs is a cloud-based Remote Web Driver which allows for a combined 96+
                        browser/OS testing
                        matrix without changing the codebase.
                        <a href="http://www.saucelabs.com">SauceLabs Homepage</a>
                    </li>
                </ul>
            </subsection>
        </section>
        <a href="#back-to-top" class="back-to-top">
            <em>Back To Top</em>
        </a>

        <a name="prerequisites" id="prerequisites"></a>
        <section name="Prerequisites">
            <p>
                In order to use and extend this framework, the following must be installed and working correctly:
            </p>
            <h4>Required Tools</h4>
            <ul>
                <li>Java JDK 1.7+ -
                    <a href="#setup">See Development Setup</a>
                </li>
                <li>Maven 3 -
                    <a href="#setup">See Development Setup</a>
                </li>
                <li>Google Chrome -
                    <a href="environment.html#chrome">See Environment Setup</a>
                </li>
                <li>
                    Chrome Driver -
                    <a href="environment.html#chrome-driver">See Environment Setup</a>
                </li>
                <li>Modern IDE
                    <ul>
                        <li>IntelliJ 11+ (community or enterprise)</li>
                        <li>Eclipse (Juno) - With Plugins: Maven (m2eclipse), Groovy and eGit</li>
                    </ul>
                </li>
            </ul>

            <h4>Optional Tools</h4>
            <p>
                The following are optional but useful tools when working with Git:
            </p>
            <ul>
                <li>Mac OSX
                    <ul>
                        <li>Source Tree - Fairly comprehensive graphical editor for git -
                            <a href="http://www.sourcetreeapp.com/">http://www.sourcetreeapp.com/</a>
                        </li>
                        <li>Git for OS X - Provides command-line tools for git -
                            <a href="http://code.google.com/p/git-osx-installer/">
                                http://code.google.com/p/git-osx-installer/
                            </a>
                        </li>
                    </ul>
                </li>
                <li>Windows
                    <ul>
                        <li>Tortoise Git - Comprehensive graphical editor for git integrated into Windows Explorer -
                            <a href="http://code.google.com/p/tortoisegit/">http://code.google.com/p/tortoisegit/</a>
                        </li>
                        <li>Git for Windows - Provides command-line tools for git including SSH tools -
                            <a href="http://code.google.com/p/msysgit/">http://code.google.com/p/msysgit/</a>
                        </li>
                        <li>SSH Agent for MsysGit - Sets up an agent to hold passphrases for SSH keys
                            <a href="https://help.github.com/articles/working-with-ssh-key-passphrases">
                                https://help.github.com/articles/working-with-ssh-key-passphrases
                            </a>
                        </li>
                        <li>Win Merge - Graphical Diff editor to help with complicated merges -
                            <a href="http://winmerge.org/">http://winmerge.org/</a>
                        </li>
                    </ul>
                </li>
            </ul>
        </section>
        <a href="#back-to-top" class="back-to-top">
            <em>Back To Top</em>
        </a>

        <section name="How to Use">
            <p>
                After all of the
                <a href="#prerequisites">prerequisites</a>
                have been met, tests may be initiated from an IDE (Eclipse or IntelliJ) or via Maven command-line
                prompt.
            </p>
            <subsection name="Run Tests (Command Line)">
                <p>
                    Run the Maven "test" phase from the command line:
                    <pre class="brush: bash;"><![CDATA[ mvn clean test ]]></pre>
                    Should produce something similar to:
                    <pre class="brush: bash; highlight: [2, 62]"><![CDATA[
user@computer ~/projects/demandware-testing-framework (master)
$ mvn clean test
[INFO] Scanning for projects...
[INFO]
[INFO] ------------------------------------------------------------------------
[INFO] Building demandware-testing-framework 0.01-SNAPSHOT
[INFO] ------------------------------------------------------------------------
[INFO]
[INFO] --- maven-clean-plugin:2.5:clean (default-clean) @ demandware-testing-framework ---
[INFO] Deleting c:\Users\user\projects\demandware-testing-framework\target
[INFO]
[INFO] --- maven-enforcer-plugin:1.2:enforce (enforce-property) @ demandware-testing-framework ---
[INFO]
[INFO] --- gmaven-plugin:1.5:generateStubs (default) @ demandware-testing-framework ---
[INFO] Generated 40 Java stubs
[INFO]
[INFO] --- maven-resources-plugin:2.6:resources (default-resources) @ demandware-testing-framework ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 42 resources
[INFO] Copying 1 resource
[INFO]
[INFO] --- jrebel-maven-plugin:1.1.3:generate (generate-rebel-xml) @ demandware-testing-framework ---
[INFO] Processing com.acquitygroup.demandware:demandware-testing-framework with packaging jar
[INFO]
[INFO] --- maven-compiler-plugin:3.0:compile (default-compile) @ demandware-testing-framework ---
[INFO] Changes detected - recompiling the module!
[INFO] Compiling 43 source files to c:\Users\user\projects\demandware-testing-framework\target\classes
[INFO]
[INFO] --- gmaven-plugin:1.5:compile (default) @ demandware-testing-framework ---
[INFO] Compiled 240 Groovy classes
[INFO]
[INFO] --- gmaven-plugin:1.5:generateTestStubs (default) @ demandware-testing-framework ---
[INFO] Generated 15 Java stubs
[INFO]
[INFO] --- maven-resources-plugin:2.6:testResources (default-testResources) @ demandware-testing-framework ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 15 resources
[INFO] Copying 2 resources
[INFO]
[INFO] --- maven-compiler-plugin:3.0:testCompile (default-testCompile) @ demandware-testing-framework ---
[INFO] Changes detected - recompiling the module!
[INFO] Compiling 15 source files to c:\Users\user\projects\demandware-testing-framework\target\test-classes
[INFO]
[INFO] --- gmaven-plugin:1.5:testCompile (default) @ demandware-testing-framework ---
[INFO] Compiled 30 Groovy classes
[INFO]
[INFO] --- maven-surefire-plugin:2.13:test (default-test) @ demandware-testing-framework ---
[INFO] Surefire report directory: c:\Users\user\projects\demandware-testing-framework\target\surefire-reports

-------------------------------------------------------
 T E S T S
-------------------------------------------------------
Running com.acquitygroup.webtest.flow.GuestCheckoutWebFlow
Started ChromeDriver
port=25195
version=26.0.1383.0
log=c:\Users\user\projects\demandware-testing-framework\chromedriver.log
Tests run: 9, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 50.753 sec

Results :

Tests run: 9, Failures: 0, Errors: 0, Skipped: 0

[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 1:07.979s
[INFO] Finished at: Sat Feb 23 17:28:55 PST 2013
[INFO] Final Memory: 37M/341M
[INFO] ------------------------------------------------------------------------

user@computer ~/projects/demandware-testing-framework (master)
$
]]></pre>
                </p>
            </subsection>

            <subsection name="Run One or more Test (IDE)">
                <ol>
                    <li>Open the Specification/TestCase you want to run</li>
                    <li>Right-click on/in the file and select "Run-As JUnit"</li>
                    <li>NOTE: For Eclipse: Use the
                        <code>@IgnoreRest</code>
                        to annotate the single
                        <em>
                            <a href="#appendix-feature">feature</a>
                        </em>
                        to test in the
                        <em>
                            <a href="#appendix-specification">specification</a>
                        </em>
                    </li>
                    <li>NOTE: IntelliJ: To run a single
                        <em>
                            <a href="#appendix-feature">feature</a>
                        </em>
                        , put the cursor inside of the method and hit "ctrl+shift+F10" to create a new run-configuration
                        specifying the single
                        <em>
                            <a href="#appendix-feature">feature</a>
                        </em>
                    </li>
                </ol>
            </subsection>
        </section>
        <a href="#back-to-top" class="back-to-top">
            <em>Back To Top</em>
        </a>

        <a name="setup" id="setup"></a>
        <section name="Development Setup">
            <a href="#environment-properties">Environment Properties</a>
            |
            <a href="#required-properties">Required Properties</a>
            |
            <a href="#optional-proeprties">Optional Properties</a>
            |
            <a href="#environment-how-to">How-To</a>
            <p>
                This project requires Java 1.7 and Maven 3.0.3+ in order to build and run the test cases. Both are
                expected to be available on the PATH and have their appropriate environment variables defined.
                Follow the directions below if the target machine does not meet these requirements
                <ol>
                    <li>Install and configure Java 1.7 -
                        <a href="environment.html#java">Directions</a>
                    </li>
                    <li>Install Maven 3 -
                        <a href="environment.html#maven">Directions</a>
                    </li>
                    <li>Eclipse
                        <ol>
                            <li>Install egit plugin -
                                <a href="eclipse.html#egit">Directions</a>
                            </li>
                            <li>Install "Groovy-Eclipse" plugin -
                                <a href="eclipse.html#groovy">Directions</a>
                            </li>
                        </ol>
                    </li>
                    <li>IntelliJ 11+ - Nothing special to do (IntelliJ Rocks!!)</li>
                    <li>Clone the Demandware Testing Framework repository:
                        <em>${gitUrl}</em>
                        <ul>
                            <li>Command line (simplest):
                                <pre class='brush: bash'><![CDATA[
cd <where you want to put the repository>
git clone ${gitUrl} ]]></pre>
                            </li>
                            <li>
                                Both IDEs provide a mechanism to clone the repository.
                                <strong>NOTE:</strong>
                                If using Eclipse, follow the
                                <a href="eclipse.html#eclipse-project-file">Eclipse Setup</a>
                                instructions to create
                                the Eclipse
                                <em>.project</em>
                                file prior to importing existing project into Eclipse.
                            </li>
                        </ul>
                    </li>
                </ol>
                <br/>
                <a href="#back-to-top" class="back-to-top">
                    <em>Back To Top</em>
                </a>

                <h2>Configuration Variables</h2>
                <a name="configuration-variables" id="configuration-variables"></a>
                <p>
                    Configuration variables are used to expose specific configuration points within the testing
                    framework allowing
                    for flexibility when using the testing framework. Configuration variables are implemented by first
                    checking
                    for an Environment Variable, then an Environment Property. If neither exist, the build will fail for
                    required
                    environment properties
                </p>

                <h3>Remote Configuration (Saucelabs)</h3>
                <p>
                    When running tests on SauceLabs a set of special environment properties are expected to configure
                    the RemoteDriver object. There are two required environment proeprties comprising the credentials
                    and one optional property to name the job for debugging.
                </p>

                <h3>Required Environment Properties</h3>
                <table>
                    <thead>
                        <tr>
                            <th>Variable</th>
                            <th>Description</th>
                            <th>Requred Scope/Use</th>
                            <th>Type</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>saucelabs.username</td>
                            <td>Username for SauceLabs account</td>
                            <td>Remote Sessions</td>
                            <td>String</td>
                        </tr>
                        <tr>
                            <td>saucelabs.password</td>
                            <td>API Key/Passphrase for SauceLabs account. Passphrase located on the account page for
                                Sauce Labs user
                            </td>
                            <td>Remote Sessions</td>
                            <td>String</td>
                        </tr>
                        <tr>
                            <td>DW_CLIENT_PASSWORD</td>
                            <td>Client ID matching OCAPI configuration</td>
                            <td>
                                <a href="ocapi.html">OCAPI Enabled Services</a>
                            </td>
                            <td>String</td>
                        </tr>
                        <tr>
                            <td>WEBDRIVER_HOME</td>
                            <td>File path location to Chrome Driver on local system</td>
                            <td>Local sessions</td>
                            <td>String</td>
                        </tr>
                        <tr>
                            <td>webdriver.chrome.driver</td>
                            <td>Alias for WEBDRIVER_HOME</td>
                            <td>Local sessions</td>
                            <td>String</td>
                        </tr>
                    </tbody>
                </table>

                <h3>Optional Environment Variables</h3>
                <table>
                    <thead>
                        <tr>
                            <th>Variable</th>
                            <th>Description</th>
                            <th>Requred Scope/Use</th>
                            <th>Type</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>saucelabs.jobname</td>
                            <td>Provide a name for SauceLabs to name the running instance (eg: BUILD_12345)</td>
                            <td>Remote Sessions</td>
                            <td>String</td>
                        </tr>
                        <tr>
                            <td>logging.webtest.level</td>
                            <td>Enables command-line level changes to the logging levels of web tests. Values can be:
                                debug, info, warn, error
                            </td>
                            <td>Anytime</td>
                            <td>String</td>
                        </tr>
                    </tbody>
                </table>
                <a href="#back-to-top" class="back-to-top">
                    <em>Back To Top</em>
                </a>

                <h2>Environment Configuration How-To</h2>
                <a name="environment-how-to" id="environment-how-to"></a>
                <h3>Command Line Variables</h3>
                <p>
                    The framework exposes some configuration changes to occur on a single run basis. These commands are
                    sent in via standard Java command line properties
                    <pre class='brush: bash'><![CDATA[
java -Dproperty.name=propertyValue -DsecondProperty=propertyValue
]]></pre>
                </p>
            </p>
        </section>
        <a href="#back-to-top" class="back-to-top">
            <em>Back To Top</em>
        </a>

        <section name="Appendix">
            <dl>
                <dt>
                    <a name="appendix-specification" id="appendix-specification"></a>
                    <dfn>Specification</dfn>
                </dt>
                <dd>A grouping of Features that comprise a Test Case. Specifications per Spock and this framework are a
                    Test Case file using the pattern "*Specification.grooyv"
                </dd>
                <dt>
                    <a name="appendix-feature" id="appendix-feature"></a>
                    <dfn>Feature</dfn>
                </dt>
                <dd>A feature is a single test within a Specification. In Spock and this framework, a feature is a
                    method inside of a Specification file
                </dd>
            </dl>
        </section>
        <a href="#back-to-top" class="back-to-top">
            <em>Back To Top</em>
        </a>

    ##        <section name="Continuous Integration">
    ##            <p>Continuous integration should run the "verify" phase in Maven</p>
    ##            <pre class="brush:bash"><![CDATA[ mvn clean verify ]]></pre>
    ##        </section>

        #include("footer.vm")

    </body>
</document>