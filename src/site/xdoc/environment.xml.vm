<?xml version="1.0" encoding="UTF-8"?>
<document xmlns="http://maven.apache.org/XDOC/2.0"
          xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
          xsi:schemaLocation="http://maven.apache.org/XDOC/2.0 http://maven.apache.org/xsd/xdoc-2.0.xsd">

    <properties>
        <title>Environment Setup - Demandware Testing Framework</title>
        <author email="mike.ensor@acquitygroup.com">Mike Ensor</author>
    </properties>

    <!-- Optional HEAD element, which is copied as is into the XHTML <head> element -->
    <head>
        <title>Environment Setup - Demandware Testing Framework</title>
        <meta content="Environment setup for Demandware Testing framework" name="description"/>

        #include("header.vm")
    </head>

    <body>
        <a name="back-to-top" id="back-to-top"></a>
        <h1>Environment Setup</h1>

        <p>
            In order to use this framework the environment in which the code is running must be setup properly. Outside of the main applications, there
            is a very minimal set of steps to setup an environment.
        </p>

        <a name="java" id="java"/>
        <section name="Setup Java 1.7">
            <p>
                Java 1.7 is required to build and run the testing framework because Groovy 2.x utilizes the JDK "invoke dynamic" instruction call introduced within Java 1.7.
                The new instructions allow for Groovy to be significantly more proficient over previous versions of Groovy. While Java 1.7 can be run along the side of previous
                Java versions this will require advanced management of environment variables. Managing multiple versions of JDK is outside the scope of this documentation.
            </p>
            <ol>
                <li>Download JDK 1.7 for machine (64-bit) -
                    <a href="http://www.oracle.com/technetwork/java/javase/downloads/index.html">Download JDK 7u15+</a>
                </li>
                <li>Install into a
                    <em>location</em>
                    - Remember the installation location
                </li>
                <li>Create an environment variable "JAVA_HOME" to point to the JDK install location -
                    <a href="http://docs.oracle.com/javase/tutorial/essential/environment/paths.html">More Documentation</a>
                </li>
                <li>Place the
                    <em>%JAVA_HOME%\bin</em>
                    at the beginning of your PATH variable
                    <ul>
                        <li>MacOSX/Linux - Modify your .bashrc or .profile and add this towards the bottom:
                            <pre class="brush: bash"><![CDATA[ export PATH=\${JAVA_HOME}/bin:\${PATH} ]]></pre>
                        </li>
                        <li>Windows - Open system environment variables, modify the "Path" variable and append "<em>%JAVA_HOME%\bin;</em>" to the beginning of the PATH string
                        </li>
                        <li>NOTE: If you are combining Java and Maven install, the PATH variable should look similar to this:
                            <ul>
                                <li>Windows:
                                    <code>%JAVA_HOME%\bin;%MAVEN_HOME%\bin;....</code>
                                </li>
                                <li>Mac/Linux:
                                    <code>\${JAVA_HOME}/bin;\${MAVEN_HOME}/bin;\${PATH}</code>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </li>
                <li>Test the install from a command prompt:
                    <pre class="brush: bash"><![CDATA[ java -version ]]></pre>
                    Should produce something similar to:
                    <pre class="brush: bash"><![CDATA[java version "1.7.0"
Java(TM) SE Runtime Environment (build 1.7.0-b17)
Java HotSpot(TM) 64-Bit Server VM (build 21.0-b17, mixed mode)
]]></pre>
                </li>
            </ol>
        </section>
        <a href="#back-to-top" class="back-to-top">
            <em>Back To Top</em>
        </a>

        <a name="maven" id="maven"></a>
        <section name="Maven 3.x Install">
            <p>
                The framework is built using Maven 3. Maven is a build tool that focuses on convention over configuration which produces a very simple source structure. In addition to project
                structure, Maven's build lifecycle is highly configurable which allows for a very simple syntax structure to build and run the tests.
            </p>
            <ol>
                <li>Download from Apache Maven 3 Download</li>
                <li>Install into a
                    <em>location</em>
                    - Remember the installation location
                </li>
                <li>Create a "MAVEN_HOME" environment variable pointing to the
                    <em>installation location</em>
                </li>
                <li>Add
                    <em>MAVEN_HOME</em>
                    to the PATH variable
                </li>
                <li>Place the %MAVEN_HOME%\bin at the beginning of your PATH variable
                    <ul>
                        <li>MacOSX/Linux - Modify your .bashrc/.profile: export PATH=\${MAVEN_HOME}/bin:\${PATH}</li>
                        <li>Windows - Open system environment variables, modify the "Path" variable and append "%MAVEN_HOME%\bin;" to the beginning of the PATH string</li>
                        <li>NOTE: If you are combining Java and Maven install, the PATH variable should look similar to this:
                            <ul>
                                <li>Windows:
                                    <em>%JAVA_HOME%\bin;%MAVEN_HOME%\bin;....</em>
                                </li>
                                <li>Mac/Linux:
                                    <em>\${JAVA_HOME}/bin;\${MAVEN_HOME}/bin;\${PATH}</em>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </li>
                <li>Test the install from a command prompt:
                    <pre class="brush: bash"><![CDATA[ mvn --version ]]></pre>
                    Should produce something similar to (paths and OS might be different):
                    <pre class="brush: bash"><![CDATA[Apache Maven 3.0.4 (r1232337; 2012-01-17 00:44:56-0800)
Maven home: c:\dev\maven
Java version: 1.7.0, vendor: Oracle Corporation
Java home: c:\dev\java-7\jre
Default locale: en_US, platform encoding: Cp1252
OS name: "windows 7", version: "6.1", arch: "amd64", family: "windows"
]]></pre>
                </li>
            </ol>
        </section>
        <a href="#back-to-top" class="back-to-top">
            <em>Back To Top</em>
        </a>

        <a name="maven-setup" id="maven-setup"></a>
        <section name="Maven Setup">
            <p>
                Once Maven and Java have been installed, Maven will need to be setup and configured for the development environment/laptop.
            </p>
            <ol>
                <li>Create/Modify your "settings.xml" file:
                    <ol>
                        <li>
                            If you are new to Maven, open/create a file named "settings.xml" in your HOME/.m2 directory (example for windows: c:\Users\me\.m2\settings.xml
                            and for Mac/Linux: ~/.m2/settings.xml))
                        </li>
                        <li>Set the contents to be:
                            <pre class='brush: xml'><![CDATA[<?xml  version="1.0" encoding="UTF-8"?>
<settings xmlns="http://maven.apache.org/SETTINGS/1.0.0"
          xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
          xsi:schemaLocation="http://maven.apache.org/SETTINGS/1.0.0
                              http://maven.apache.org/xsd/settings-1.0.0.xsd">

    <activeProfiles>
        <activeProfile>acquity-group</activeProfile>
    </activeProfiles>

    <profiles>
        <profile>
            <id>acquity-group</id>
            <properties>
                <!-- PhantomJS for Ghost Driver -->
                <path.to.phantomjs/>
                <!-- Change this for linux vs windows -->
                <!-- Example:  C:\dev\chromedriver\chromedriver.exe  or  ~/dev/chomedriver/chromedriver -->
                <!-- Or as an environment variable -->
                <path.to.webdriver>\${env.WEBDRIVER_HOME}</path.to.webdriver>
            </properties>
        </profile>
    </profiles>
</settings>
]]></pre>
                        </li>
                    </ol>
                </li>
                <li>Establish the maven property that points to the Chrome Driver location: NOTE: If you have an environment variable named "WEBDRIVER_HOME", skip this step
                    <ul>
                        <li>
                            Based on the above settings.xml file, set the
                            <strong>"path.to.webdriver"</strong>
                            property to the location of the Chrome Driver executable
                        </li>
                    </ul>
                </li>
            </ol>
        </section>
        <a href="#back-to-top" class="back-to-top">
            <em>Back To Top</em>
        </a>

        <a name="chrome" id="chrome"></a>
        <section name="Chrome">
            <p>This framework is best run using Chrome via Chrome Browser since the browser can be run in incognito mode for maximum load time</p>
            <ol>
                <li>Download and install Chrome Browser -
                    <a href="http://www.google.com/chrome">Download Here</a>
                </li>
            </ol>
        </section>
        <a href="#back-to-top" class="back-to-top">
            <em>Back To Top</em>
        </a>

        <a name="chrome-driver" id="chrome-driver"></a>
        <section name="ChromeDriver Setup">
            <p>
                This framework uses WebDriver which requires a small application called a
                <em>driver</em>
                to act as an interface between browsers and the code. This decoupling provides the
                ability to use the same code for multiple browsers, even remote browsers via RemoteWebDriver.
            </p>
            <ol>
                <li>Download ChromeDriver -
                    <a href="http://code.google.com/p/chromedriver/downloads/list">Download Here</a>
                </li>
                <li>Unpack the compressed artifact into a<em>known location</em>. NOTE: Remember this location
                </li>
                <li>Create an
                    <em>environment variable</em>
                    named WEBDRIVER_HOME
                </li>
                <li>Set the value to the location of the executable from step #1</li>
                <li>Save and close</li>
            </ol>
        </section>
        <a href="#back-to-top" class="back-to-top">
            <em>Back To Top</em>
        </a>

        #include("footer.vm")

    </body>
</document>