package com.acquitygroup.webtest.page.home

import com.acquitygroup.webtest.module.navigation.HeaderModule
import com.acquitygroup.webtest.module.navigation.NavigationModule
import geb.Page
import org.slf4j.Logger
import org.slf4j.LoggerFactory

/**
 * Homepage for a Site Genesis Implementation
 */
class ChildHomePage extends HomePage {

    private static final Logger LOG = LoggerFactory.getLogger(ChildHomePage.class)

    /**
     * Base URL
     */
    static url = "default/CustomerService-Show"

    /**
     * Title for Site Genesis
     */
    static at = {
        LOG.debug("at() in Child Home Page")
        title.trim() == "Sites-"+browser.config.rawConfig.siteName+"-Site"
    }

    /**
     * Content Variables available on the HomePage
     * <ul>
     *      <li>headerModule - Links to the {@link HeaderModule}</li>
     *      <li>navigationModule - Links to the {@link NavigationModule}</li>
     * </ul>
     */
    static content = {
        /**
         * BLAH BLAH
         */
        headerModule { module HeaderModule }
        navigationModule { module NavigationModule }
    }

}
