package com.acquitygroup.webtest.page.integrations.paypal

import geb.Page

class PayPalLoginPage extends Page {

	static url = "https://www.sandbox.paypal.com/cgi-bin/webscr?cmd=_express-checkout&token="

	static at = {
		title.trim() == "Pay with a PayPal account - PayPal"
	}

	static content = {
	loginBox { $("div#loginBox") }
	emailInput { loginBox.find("input#login_email") }
	passwordInput { loginBox.find("input#login_password") }
	loginButton {loginBox.find("input#submitLogin") }
	PPcontinueButton (required: false) {$("input#continue")}

	}

	def loginDWcustomer(){
		emailInput = ""
		passwordInput = ""
		emailInput << "acquitydwpersonal@gmail.com"
		passwordInput << "Acquity2013"
		loginButton.click()
	}
}
