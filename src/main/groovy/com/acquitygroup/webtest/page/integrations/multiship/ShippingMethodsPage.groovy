package com.acquitygroup.webtest.page.integrations.multiship

import com.acquitygroup.webtest.model.Address;

import geb.Page

class ShippingMethodsPage extends Page {

	static url = "default/Cart-Show"

	static at = {
		title.trim() == "Sites-RefSite-Site"
	}

	static content = {
		shipmentContainer {$("div.checkoutmultishipping")}
		shippingMethodSelect {String x -> shipmentContainer.find("select", name: startsWith("dwfrm_multishipping_shippingOptions_shipments_i${x}_shippingMethodID"))}
		
		continueButton{ $("button", value: startsWith("Continue"), type: "submit") }
		
	}
	

}
