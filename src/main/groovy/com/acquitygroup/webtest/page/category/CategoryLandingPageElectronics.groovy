package com.acquitygroup.webtest.page.category

import com.acquitygroup.webtest.page.product.ProductDetailsPage
import geb.Page;
@Deprecated
class CategoryLandingPageElectronics extends Page {
    static url = "default/Search-Show?cgid=electronics"
    static at = { header }

    static content = {
        header(wait: true, required: false) {
            $("span", class: "refinement-header", text: startsWith("SHOP ELECTRONICS"), 0)
        }

        product { String title -> $("a.thumb-link img", title: startsWith(title)) }

        productDetailsLink(to: ProductDetailsPage) { String title -> $("a.name-link", title: startsWith(title), 0) }

    }
}
