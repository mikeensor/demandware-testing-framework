package com.acquitygroup.webtest.page.category

import com.acquitygroup.webtest.model.category.CategoryLevel
import com.acquitygroup.webtest.page.product.ProductDetailsPage;

import geb.Page

class CategoryLandingPage extends Page {

    static url = "default/Search-Show"
    static at = { header }

    static content = {
        header(wait: true, required: false) { $("span", class: "refinement-header", text: startsWith("SHOP "), 0) }

		product { String title -> $("div.product-tile a.thumb-link", title: startsWith(title)) }

        productQuickViewLink { String title -> product(title).find("img") }

        // TODO: Is this the same as product??
		productDetailsLink(to: ProductDetailsPage) { String title -> $("a.name-link", title: startsWith(title), 0) }
    }

    @Override
    String convertToPath(Object[] args) {
        String query = ""

        if (isArgsACategoryObject(args)) {
            query = ((CategoryLevel) args[0]).getQueryString()
        }

        query
    }

    private boolean isArgsACategoryObject(Object[] args) {
        args != null && args.size() > 0 && args[0] instanceof CategoryLevel
    }
}
