package com.acquitygroup.webtest.page.checkout

import com.acquitygroup.webtest.model.account.Customer
import geb.Page

class CheckoutLoginPage extends Page {

    static url = "default/Cart-Show"

    static at = {
        title.trim() == "My SiteGenesis Account Login"
    }

    static content = {

        checkoutLogin { $("form#dwfrm_login") }
        checkoutEmail { checkoutLogin.find("input", id: startsWith("dwfrm_login_username_")) }
        checkoutPassword { checkoutLogin.find("#dwfrm_login_password") }
        checkoutSignIn { checkoutLogin.find("button", name: ("dwfrm_login_login")) }

        checkoutAsGuest { $("div.guest-checkout button.primary-btn") }

    }

    def loginUser(Customer customer) {
        checkoutEmail = customer.getEmail()
        checkoutPassword = customer.getPassword()
        checkoutSignIn.click()
    }
}

