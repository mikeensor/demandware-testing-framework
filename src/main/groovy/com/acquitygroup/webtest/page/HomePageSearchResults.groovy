package com.acquitygroup.webtest.page

import geb.Page

class HomePageSearchResults extends Page {
    static url = "default/Search-Show?q=&simplesearch=Go"
    static at = { title.trim() == "SiteGenesis Online Store" }
}