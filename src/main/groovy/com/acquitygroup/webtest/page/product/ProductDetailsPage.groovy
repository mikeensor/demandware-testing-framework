package com.acquitygroup.webtest.page.product

import com.acquitygroup.webtest.model.product.Product
import com.acquitygroup.webtest.module.product.ProductDetailsModule
import geb.Page
import org.slf4j.Logger
import org.slf4j.LoggerFactory

/**
 * Product Details page is a dynamically rendered page that when the initial page is first rendered, each call for the color update is done via AJAX.  Made this a tricky verification.  See the
 * dynamic selector for current color in the module
 */
class ProductDetailsPage extends Page {

    private static final Logger LOG = LoggerFactory.getLogger(ProductDetailsPage.class)

    @Override
    String convertToPath(Object[] args) {
        String query = ""

        if (isArgsAProductObject(args)) {
            query = ((Product) args[0]).getQueryString()
        }

        LOG.debug("Convert To Path Query: {}" , query)

        query
    }

    /**
     * TODO: There is a different page for Product-Show that handles updates from cart
     * TODO: Sample URL: default/Product-Show?pid=793775370033-1&Quantity=1&uuid=bdLUkiaag0qgYaaac6wd6xWbfJ&source=cart
     *
     */
    static url = "default/Product-Show"

    static at = {
        // TODO: How do we make this parametertized so we can not use the one product?
//        title.trim().startsWith "Checked Silk Tie"
        productContainer
    }

    static content = {
        productDetails(cache: false) { module ProductDetailsModule }
        productContainer { $("#pdpMain") }
    }

    protected boolean isArgsAProductObject(Object[] args) {
        boolean isProduct = args != null && args.size() > 0 && args[0] instanceof Product

        LOG.debug("isArgsAProductObject: {}" , isProduct)

        isProduct
    }

}
