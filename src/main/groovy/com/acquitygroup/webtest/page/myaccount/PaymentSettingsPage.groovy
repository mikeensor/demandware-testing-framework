package com.acquitygroup.webtest.page.myaccount

import geb.Page

import com.acquitygroup.webtest.model.CreditCard;
import com.acquitygroup.webtest.module.myaccount.PaymentsModule;

class PaymentSettingsPage extends Page {

	static url = "default/PaymentInstruments-List";
	
	static at = {
		waitFor {
			title.equals("My SiteGenesis Payment Settings")
		}
	}
	
	static content = {
		addCreditCard { $("a.section-header-note.dialogify") }
		deleteCreditCard { $("payment-list a.delete") }
		paymentModule { module PaymentsModule }
		cardList (required: false) { $("ul.payment-list") }
		firstCard (required: false) {String contents -> cardList.find("li.first", 0).text().contains(contents) }
		
	}
	def deleteCard(CreditCard card){
		withConfirm(true) { $("ul.payment-list li.first", text: contains(card.getName())).parent().find(".delete").click() }
	}
	
	def isCardAvailable(CreditCard card){
		$("ul.payment-list li.first", text: contains(card.getName())).size() != 0
	}
}
