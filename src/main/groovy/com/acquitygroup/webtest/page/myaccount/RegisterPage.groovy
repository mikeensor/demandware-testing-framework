package com.acquitygroup.webtest.page.myaccount

import com.acquitygroup.webtest.module.myaccount.RegisterModule
import geb.Page

class RegisterPage extends Page {

    static url = "default/Account-StartRegister";

    static at = {
        registerForm.primaryHeader.text().contains("Create Account")
    }

    static content = {
        registerForm { module RegisterModule }
    }

}
