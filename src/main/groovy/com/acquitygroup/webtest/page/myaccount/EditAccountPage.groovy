package com.acquitygroup.webtest.page.myaccount

import geb.Page

class EditAccountPage extends Page {

	static url = "default/Account-EditProfile";
	
	static at = {
		waitFor {
			title.equals("My SiteGenesis Edit Account")
		}
	}
	
	static content = {
		registerForm { module RegisterModule }
	}
}
