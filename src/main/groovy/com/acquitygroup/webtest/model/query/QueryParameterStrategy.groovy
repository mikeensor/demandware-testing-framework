package com.acquitygroup.webtest.model.query

import com.acquitygroup.webtest.model.product.Product

public interface QueryParameterStrategy {

    String getQueryString(Product product);

}