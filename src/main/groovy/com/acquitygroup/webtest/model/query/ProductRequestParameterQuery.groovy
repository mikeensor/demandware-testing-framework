package com.acquitygroup.webtest.model.query

import com.acquitygroup.webtest.model.product.Product
import com.acquitygroup.webtest.model.product.ProductAttribute
import org.slf4j.Logger
import org.slf4j.LoggerFactory

class ProductRequestParameterQuery implements QueryParameterStrategy {

    private static final Logger LOG = LoggerFactory.getLogger(ProductRequestParameterQuery.class)

    @Override
    String getQueryString(Product product) {
        //Product-Show?pid=12416789&dwvar_12416789_color=GYX#cgid=newarrivals-mens&start=1
        //Product-Variation?pid=12416789&dwvar_12416789_size=30&dwvar_12416789_color=GYX

        def pidParam = "pid=${product.getPid().getPid()}"
        def startParam = product.getStart() ? "start=${product.getStart()}" : null
        def dwvar = [:]

        product.getAttributeMap().each { String mapKey, ProductAttribute attribute ->
            String key = attribute.getKey()
            String value = attribute.getAttributeValue()
            dwvar[key] = getDvarString(product.getPid().getVariantId(), key, value)
        }

        def dwvarString = dwvar.inject([]) { result, entry -> result << "${entry.value}" }.join('&')

        if (product.getCategoryUrlSegment()) {
            dwvarString += "#cgid=${product.getCategoryUrlSegment()}"
        }

        def each = [pidParam, dwvarString, startParam].findAll() { str ->
            if (str != null) {
                str
            }
        }
        String query = "?" + each.join("&").toString()

        LOG.debug(query)

        query
    }

    private static String getDvarString(String variantId, String key, String value) {
        "dwvar_${variantId}_${key}=${value}"
    }

}
