package com.acquitygroup.webtest.model.query

import com.acquitygroup.webtest.model.product.Product

class EmptyQueryStrategy implements QueryParameterStrategy {

    @Override
    String getQueryString(Product product) {
        ""
    }
}
