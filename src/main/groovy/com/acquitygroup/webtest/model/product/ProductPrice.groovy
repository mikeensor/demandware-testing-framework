package com.acquitygroup.webtest.model.product

class ProductPrice implements ProductAttribute {

    private final BigDecimal salesPrice
    private final BigDecimal basePrice

    ProductPrice(BigDecimal basePrice, BigDecimal salesPrice) {
        this.basePrice = basePrice
        this.salesPrice = salesPrice
    }

    BigDecimal getSalesPrice() {
        return salesPrice
    }

    BigDecimal getBasePrice() {
        return basePrice
    }

    @Override
    String getAttributeValue() {
        return getBasePrice()
    }

    @Override
    String getKey() {
        "price"
    }
}
