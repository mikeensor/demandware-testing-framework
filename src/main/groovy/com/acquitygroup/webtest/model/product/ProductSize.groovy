package com.acquitygroup.webtest.model.product

class ProductSize implements ProductAttribute {

    private final String display
    private final String data

    ProductSize(String display, String data) {
        this.display = display
        this.data = data
    }

    ProductSize(String display) {
        this.display = display
        this.data = display
    }

    String getDisplay() {
        display
    }

    String getData() {
        data
    }

    @Override
    String getAttributeValue() {
        getData()
    }

    @Override
    String getKey() {
        "size"
    }
}
