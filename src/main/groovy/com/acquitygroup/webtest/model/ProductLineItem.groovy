package com.acquitygroup.webtest.model

import com.acquitygroup.webtest.model.product.Product

/**
 * Model class encapsulating what a ProductLineItem is in a default Demandware site
 */
class ProductLineItem {
    private final Product product
    private final Integer quantity

    /**
     * Constructs a full ProductLineItem
     * @param product
     * @param quantity
     */
    ProductLineItem(Product product, Integer quantity) {
        this.product = product
        this.quantity = quantity
    }

    Integer getProduct() {
        return product
    }

    Integer getQuantity() {
        return quantity
    }
}
