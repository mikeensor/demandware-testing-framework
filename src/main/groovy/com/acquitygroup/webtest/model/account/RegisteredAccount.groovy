package com.acquitygroup.webtest.model.account

class RegisteredAccount {

    public static RegisteredAccount INVALID = new RegisteredAccount("INVALID", "INVALID", null)

    private final String customerNumber
    private final String email
    private final AccountProfile profile

    RegisteredAccount(String customerNumber, String email, AccountProfile profile) {
        this.customerNumber = customerNumber
        this.email = email
        this.profile = profile
    }

    String getCustomerNumber() {
        return customerNumber
    }

    String getEmail() {
        return email
    }

    AccountProfile getProfile() {
        return profile
    }

    boolean equals(o) {
        if (this.is(o)) {
            return true
        }
        if (getClass() != o.class) {
            return false
        }

        RegisteredAccount that = (RegisteredAccount) o

        if (getCustomerNumber() != that.getCustomerNumber()) {
            return false
        }

        if (getEmail() != that.getEmail()) {
            return false
        }

        true
    }


}
