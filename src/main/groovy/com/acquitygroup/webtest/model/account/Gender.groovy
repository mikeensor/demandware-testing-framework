package com.acquitygroup.webtest.model.account

enum Gender {
    MALE("m", 1), FEMALE("f", 2), UNKNOWN("u", 3)

    private String mnemonic
    private int value

    Gender(String mnemonic, int value) {
        this.mnemonic = mnemonic
        this.value = value
    }

    public static Gender getGender(String genderMnemonic) {
        Gender foundGender = UNKNOWN

        for(Gender g: values()) {
            if(g.getMnemonic().equalsIgnoreCase(genderMnemonic)) {
                foundGender = g
                break;
            }
        }
        foundGender
    }

    public String getMnemonic() {
        return mnemonic
    }

    int getValue() {
        return value
    }
}
