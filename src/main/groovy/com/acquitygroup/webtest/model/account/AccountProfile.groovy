package com.acquitygroup.webtest.model.account

class AccountProfile {

    private final String first
    private final String last
    private final String email
    private final String password
    private final Gender gender
    private final String dateOfBirth
    private final String preferredLocale

    /**
     * Helper constructor for English US locale
     *
     * @param first
     * @param last
     * @param email
     * @param password
     * @param gender
     * @param dateOfBirth
     */
    AccountProfile(String first, String last, String email, String password, Gender gender, String dateOfBirth = "2011-05-06") {
        this(first, last, email, password, gender, dateOfBirth, "en-US")
    }

    AccountProfile(String first, String last, String email, String password, Gender gender, String dateOfBirth, String preferredLocale) {
        this.first = first
        this.last = last
        this.email = email
        this.password = password
        this.gender = gender
        this.dateOfBirth = dateOfBirth
        this.preferredLocale = preferredLocale
    }

    String getFirst() {
        first
    }

    String getLast() {
        last
    }

    String getEmail() {
        email
    }

    String getPassword() {
        password
    }

    Gender getGender() {
        gender
    }

    String getDateOfBirth() {
        dateOfBirth
    }

    String getPreferredLocale() {
        preferredLocale
    }
}
