package com.acquitygroup.webtest.model.category

import com.acquitygroup.webtest.model.query.Queryable

abstract class CategoryLevel implements Queryable {

    protected final String category
    protected final String categoryTitle

    CategoryLevel(String category, String categoryTitle) {
        this.category = category
        this.categoryTitle = categoryTitle
    }

    String getCategory() {
        category
    }

    String getTitle() {
        categoryTitle
    }

    @Override
    abstract String getQueryString()

}
