package com.acquitygroup.webtest.rest

class AuthorizationException extends RuntimeException {

    AuthorizationException(String message) {
        super(message)
    }
}
