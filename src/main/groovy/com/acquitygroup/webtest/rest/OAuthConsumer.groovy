package com.acquitygroup.webtest.rest

import com.google.common.io.BaseEncoding

class OAuthConsumer {

    private final String clientId
    private final String password

    OAuthConsumer(String clientId, String password) {
        this.clientId = clientId
        this.password = password
    }

    String getClientId() {
        return clientId
    }

    String getPassword() {
        return password
    }

    String getAuthentication() {
        String userPass = clientId + ":" + password
        String encoded = BaseEncoding.base64().encode(userPass.getBytes())
        encoded
    }
}
