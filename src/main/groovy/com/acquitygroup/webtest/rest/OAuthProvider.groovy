package com.acquitygroup.webtest.rest

import com.google.common.collect.ImmutableMap
import groovy.json.JsonSlurper
import groovyx.net.http.ContentType
import groovyx.net.http.HttpResponseDecorator
import groovyx.net.http.RESTClient
import org.apache.http.HttpStatus
import org.slf4j.Logger
import org.slf4j.LoggerFactory

class OAuthProvider {

    private static final Logger LOG = LoggerFactory.getLogger(OAuthProvider.class)

    private final String authUrl

    OAuthProvider(String authUrl) {
        this.authUrl = authUrl
    }

    String getAuthUrl() {
        return authUrl
    }

    String retrieveToken(OAuthConsumer oAuthConsumer) {
        RESTClient client = new RESTClient(authUrl)
        client.setContentType(ContentType.URLENC)
        client.setHeaders(ImmutableMap.of(
                "Content-Type", "application/x-www-form-urlencoded",
                "Authorization", "Basic " + oAuthConsumer.getAuthentication()
        ))

        def grantType = ["grant_type": "client_credentials"]
        String token = null
        try {
            HttpResponseDecorator resp = client.post(body: grantType) as HttpResponseDecorator
            if (resp.getStatus() == HttpStatus.SC_OK) {
                Map data = resp.getData()
                LOG.debug(data.toString())
                if (data != null) {
                    JsonSlurper slurper = new JsonSlurper()
                    String jsonString = data.keySet().toArray()[0]
                    def json = slurper.parseText(jsonString)
                    token = json.access_token
                }
            }
        } catch (ex) {
            LOG.error("Failed to retrieve OAuth Token for {}", getAuthUrl())
            LOG.error("Response Data: {}", ex.response.getData())
            ex.printStackTrace()
        }

        token
    }
}
