package com.acquitygroup.webtest.service

import com.google.common.annotations.VisibleForTesting

public abstract class AbstractStoreService extends AbstractRemoteService {

    private static final String SHOP_RESOURCE = '/shop'

    public AbstractStoreService() {
        super()
    }

    @VisibleForTesting
    AbstractStoreService(URI configFilePath) {
        super(configFilePath)
    }

    @Override
    String getResource() {
        SHOP_RESOURCE
    }

}
