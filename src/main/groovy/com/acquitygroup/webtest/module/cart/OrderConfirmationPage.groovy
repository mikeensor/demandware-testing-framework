package com.acquitygroup.webtest.module.cart

class OrderConfirmationPage {

    static url = "default/COSummary-Submit"

    static at = {
        title.trim() == "Checkout Order Confirmation"
    }

    static content = {
        orderDetails() { $("div.orderconfirmation_details") }
        orderNumber() { orderDetails.find("div.ordernumber span.value", 0) }
    }
}
