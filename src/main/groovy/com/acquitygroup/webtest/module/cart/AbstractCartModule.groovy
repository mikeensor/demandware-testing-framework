package com.acquitygroup.webtest.module.cart

import com.acquitygroup.webtest.model.product.Product
import geb.Module

abstract class AbstractCartModule extends Module {

    static content = {
        subTotal { assert false }
        productTitle { assert false }
        productQuantity { assert false }
        productSalePrice { assert false }
        productLineItemTitle { assert false }
        productList { assert false }
    }

    abstract boolean isProductInCart(Product product)
}
