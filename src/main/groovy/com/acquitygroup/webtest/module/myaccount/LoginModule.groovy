package com.acquitygroup.webtest.module.myaccount

import com.acquitygroup.webtest.page.myaccount.MyAccountPage
import geb.Module

class LoginModule extends Module {

    static content = {

        emailAddress { $("input", type: "text", class: "input-text email-input") }
        emailAddressError(required: false, wait: true) {
            // TODO: there's gotta be a better way to do this, I'd like to tie the "email address" and the field error
            $("input", type: "text", class: "input-text email-input").next("span").text()
        }

        password { $("#dwfrm_login_password") }
        passwordError(required: false, wait: true) {
            $("#dwfrm_login_password + span.error").text()
        }

        rememberMe { $("#dwfrm_login_rememberme") }

        login(to: MyAccountPage) { $("button", name: "dwfrm_login_login", type: "submit") }
        logoImage { $("h1.primary-logo a") }

    }

    /**
     * Performs the Login and Password when on the Login Page
     * @param email
     * @param password
     * @return
     */
    def fillAndSubmitLogin(String email, String inPassword) {
        //assert page.at(LoginPage)
        emailAddress.click()
        emailAddress << email
        password << inPassword
        login.click()
    }

    def getFormErrors() {
        // TODO: Is there a chance there could be multiple error divs?  If so, create a module and pass a base?
        $("#dwfrm_login").find("div.error-form")*.text()
    }

}
