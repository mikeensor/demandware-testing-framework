package com.acquitygroup.webtest.module.navigation

import geb.Module

class NavigationModule extends Module {
    static content = {
        // @deprecated - Use topLevelNavigation instead
        level1Electronics { $("li a.level-1", text: "ELECTRONICS") }
        // @deprecated - Use topLevelNavigation instead
        level1NewArrivals { $("li.first a.level-1", text: "NEW ARRIVALS") }

        topLevelNavigation { String navigationText -> $("li a.level-1", text: contains(navigationText)) }
        miniCartLink { $("span.mini-cart-label a") }
    }

    public void showNewArrivals() {
        $("li.first a.level-1", text: "NEW ARRIVALS").jquery.mouseover()
        waitFor { level2Womens }
    }
}
