package com.acquitygroup.webtest.module.navigation

import geb.Module
import org.slf4j.Logger
import org.slf4j.LoggerFactory

class BreadcrumbModule extends Module {

    private static final Logger LOG = LoggerFactory.getLogger(BreadcrumbModule.class)

    static content = {
        list { $("ol.breadcrumb li a") } // should be the same CSS Selector for all pages
    }

    /**
     * Checks to see if the incoming List of expected ordered breadcrumb text are the same as the page
     * @param expectedBreadcrumb List<String> text from left to right representing the breadcrumbs
     * @return boolean
     */
    def verifyWithPage(def expectedBreadcrumb) {
        for (def i = 0; i < list.size(); i++) {
            LOG.debug("Page[{}] == Expected[{}]", list[i].text(), expectedBreadcrumb[i])
            assert list[i].text() == expectedBreadcrumb[i]
        }
        true
    }

}
