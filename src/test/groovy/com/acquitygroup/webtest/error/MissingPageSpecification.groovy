package com.acquitygroup.webtest.error

import com.acquitygroup.webtest.page.error.MissingPage
import com.acquitygroup.webtest.page.product.ProductDetailsPage
import com.clickconcepts.geb.OverridableGebReportingSpec
import spock.lang.Ignore

/**
 * This is testing the missing (404) page by going to the product page without a proper sku
 */
@Ignore("UnexpectedPageException handling is not working as expected")
class MissingPageSpecification extends OverridableGebReportingSpec {

    def "display page not found for the product page without a product(SKU) "() {

        when: "attempt to reach product details page with no SKU"
        via ProductDetailsPage

        then:
        at MissingPage

    }
}
