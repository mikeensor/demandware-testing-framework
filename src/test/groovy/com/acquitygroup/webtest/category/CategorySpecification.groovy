package com.acquitygroup.webtest.category

import com.acquitygroup.webtest.model.category.CategoryLevel
import com.acquitygroup.webtest.model.category.QueryParamCategoryLevel
import com.acquitygroup.webtest.page.category.CategoryLandingPage
import geb.spock.GebReportingSpec
import org.slf4j.Logger
import org.slf4j.LoggerFactory

class CategorySpecification extends GebReportingSpec {

    private static final Logger LOG = LoggerFactory.getLogger(CategorySpecification.class)

    def "when on category page click the quickview to go to a product detail page"() {
        CategoryLevel category = new QueryParamCategoryLevel("electronics", "Electronics")
        given: "go to category landing page"
        to CategoryLandingPage, category

        expect: "at category landing page"
        at CategoryLandingPage

        when: "user clicks on product tile"
        productQuickViewLink("Apple iPod Touch").jquery.mouseover().click()

        waitFor { $("span.ui-dialog-title") }

        then: "quickview appears"
        $("span.ui-dialog-title").text() == "PRODUCT QUICKVIEW"

        LOG.debug("Navigating to Electronics Landing Page test done...")
    }

    def "Goto a Top Level Landing Page"() {
        CategoryLevel category = new QueryParamCategoryLevel("womens", "Womens")

        given: "go to category landing page"
        to CategoryLandingPage, category

        expect: "at category landing page"
        at CategoryLandingPage
    }

}

