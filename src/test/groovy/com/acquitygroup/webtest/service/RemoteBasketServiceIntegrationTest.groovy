package com.acquitygroup.webtest.service

import com.acquitygroup.webtest.model.product.Product
import spock.lang.Specification

import static org.fest.assertions.api.Assertions.assertThat

class RemoteBasketServiceIntegrationTest extends Specification {

    private RemoteBasketService service
    private RemoteProductService productService

    def setup() {
        URI config = getClass().getResource('/OCapiTestConfig.groovy').toURI()

        productService = new RemoteProductService(config)
        service = new RemoteBasketService(config)
    }

    def "clear basket session"() {
        when:
        service.clearBasketSession()

        then:
        !service.hasBasketSession()
    }

    def "addProductToBasket"() {
        setup:
        Product product = productService.getProductById("682875540326-1")
        assertThat(product).isNotNull()

        when:
        def resultObject = service.addProductToBasket(product)

        then:
        resultObject != null
        resultObject.product_items.size() == 1
        resultObject.product_items[0].product_id == product.getPid().getPid()
    }

    def "add to products to basket"() {
        setup:
        Product product = productService.getProductById("682875540326-1")
        assertThat(product).isNotNull()

        when:
        def resultObject = service.addProductToBasket(product)

        then:
        resultObject != null
        resultObject.product_items.size() == 1
        resultObject.product_items[0].product_id == product.getPid().getPid()

        when:
        Product secondProduct = productService.getProductById("682875090845")
        assertThat(secondProduct).isNotNull()
        resultObject = service.addProductToBasket(secondProduct)

        then:
        resultObject.product_items.size() == 2
    }


}