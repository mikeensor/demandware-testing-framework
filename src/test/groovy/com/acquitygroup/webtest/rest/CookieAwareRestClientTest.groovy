package com.acquitygroup.webtest.rest

import spock.lang.Specification

class CookieAwareRestClientTest extends Specification {

    def "test authenticate"() {
        given:
        CookieAwareRestClientGroovy client = new CookieAwareRestClientGroovy("https://acquitygroup01.alliance-prtnr-na01.dw.demandware.net/")
        when:
        client.setAuthenticationToken()
        then:
        client.getAuthToken() != null
    }
}
