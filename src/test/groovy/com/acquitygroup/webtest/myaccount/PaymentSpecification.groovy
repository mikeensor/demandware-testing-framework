package com.acquitygroup.webtest.myaccount

import com.acquitygroup.webtest.model.CreditCard
import com.acquitygroup.webtest.model.account.Gender
import com.acquitygroup.webtest.model.account.RegisteredAccount
import com.acquitygroup.webtest.model.account.AccountProfile
import com.acquitygroup.webtest.page.myaccount.LoginPage
import com.acquitygroup.webtest.page.myaccount.MyAccountPage
import com.acquitygroup.webtest.page.myaccount.PaymentSettingsPage
import com.acquitygroup.webtest.service.RemoteAccountService
import com.acquitygroup.webtest.service.RemoteStoreAccountService
import com.acquitygroup.webtest.util.AccountUtil
import com.acquitygroup.webtest.util.RandomUtil
import geb.Browser
import geb.spock.GebReportingSpec
import spock.lang.Shared
import spock.lang.Stepwise

@Stepwise
class PaymentSpecification extends GebReportingSpec {

    @Shared
    protected RemoteStoreAccountService storeAccountService

    @Shared
    protected RemoteAccountService bulkAccountService

    @Shared
    protected RegisteredAccount customer

    protected CreditCard card = new CreditCard("New Belgium", "Visa", "4111111111111111", "October", 2017, 876)

    /**
     * Always create one account for the whole specification
     */
    def setupSpec() {
        expect: "create account"
        URI config = getClass().getResource('/OCapiTestConfig.groovy').toURI()
        storeAccountService = new RemoteStoreAccountService(config)
        bulkAccountService = new RemoteAccountService(config)

        String first = "First"
        String last = "Last"
        String email = "${first}.${last}_" + RandomUtil.getInt(10000) + "@mailinator.com".toLowerCase()

        String password = "p@\$\$w0rd1!"
        AccountProfile account = new AccountProfile(first, last, email, password, Gender.MALE)

        customer = storeAccountService.registerCustomer(account)

        assert customer != RegisteredAccount.INVALID
        assert customer.getProfile().getPassword() == password

        when: "Login to browser with new user"
        to LoginPage
        loginModule.fillAndSubmitLogin(customer.getEmail(), customer.getProfile().getPassword())

        then:
        at MyAccountPage
    }

    /**
     * Logs out a customer
     * @return
     */
    def cleanupSpec() {
        expect: "log out a user"
        AccountUtil.logout(getBrowser())
        assert bulkAccountService.removeAccount(customer.getCustomerNumber()) == true
    }

    def "user can add a new payment method"() {
        when: "go to payment settings page"
        to PaymentSettingsPage

        then: "user is at payment settings page"
        at PaymentSettingsPage

        if (!isCardAvailable(card)) {
            addCard(getBrowser(), card)
        }

    }

    def "user is presented with error when a required field is not populated"() {
        given: "go to payment settings page"
        to PaymentSettingsPage

        expect: "user is at payment settings page"
        at PaymentSettingsPage

        when: "customer clicks to create new address"
        addCreditCard.click()

        and: "payment form is available"
        with(paymentModule) {
            waitFor { paymentForm.present }
        }
        paymentModule.applyButton.click()
        then: "ensure error is displayed"
        waitFor { paymentModule.requiredFieldError.present }
    }

    def "user can delete an existing card"() {
        given: "go to payment settings page"
        to PaymentSettingsPage

        expect: "user is at payment settings page"
        at PaymentSettingsPage
        if (!isCardAvailable(card)) {
            addCard(getBrowser(), card)
        }
        when: "click delete card"
        // find card that we created earlier and click "delete"
        waitFor { deleteCard(card) }

        then: "ensure card is deleted"
        to PaymentSettingsPage
        at PaymentSettingsPage
        waitFor { !isCardAvailable(card) }
    }

    def addCard(Browser browser, CreditCard localCard) {
        with(browser) {
            expect: "user is at payment settings apge"
            at PaymentSettingsPage

            when: "customer clicks to create new payment method"
            addCreditCard.click()

            and: "payment form is available"
            with(paymentModule) {
                waitFor { paymentForm.present }
            }
            paymentModule.fillPaymentForm(localCard)
            paymentModule.applyButton.click()
            then: "new payment method is added"
            to PaymentSettingsPage
            and: "ensure card is available"
            isCardAvailable(localCard)
        }
        assertCardInformation(localCard)
    }

    //verify payment information
    def assertCardInformation(CreditCard localCard) {
        firstCard(localCard.getName())
        firstCard(localCard.getType())
        firstCard(localCard.getNumber().substring(localCard.getNumber().length() - 4))
        firstCard(localCard.getExpiryYear().toString())
    }

}