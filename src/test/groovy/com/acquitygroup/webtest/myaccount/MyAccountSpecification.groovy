package com.acquitygroup.webtest.myaccount

import com.acquitygroup.webtest.model.account.Customer
import com.acquitygroup.webtest.model.account.RegisteredAccount
import com.acquitygroup.webtest.page.myaccount.LoginPage
import com.acquitygroup.webtest.page.myaccount.LogoutPage
import com.acquitygroup.webtest.page.myaccount.RegisterPage
import com.acquitygroup.webtest.page.myaccount.RegistrationResultPage
import com.acquitygroup.webtest.service.RemoteAccountService
import com.acquitygroup.webtest.util.RandomUtil
import com.clickconcepts.geb.OverridableGebReportingSpec
import org.openqa.selenium.Keys
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import spock.lang.Shared

class MyAccountSpecification extends OverridableGebReportingSpec {

    private static final Logger LOG = LoggerFactory.getLogger(MyAccountSpecification.class)

    @Shared
    protected RemoteAccountService bulkAccountService

    protected Customer customer

    def setupSpec() {
        URI config = getClass().getResource('/OCapiTestConfig.groovy').toURI()
        bulkAccountService = new RemoteAccountService(config)
    }

    def setup() {
        String randomPostFix = RandomUtil.getInt(100_000)
        def email = "first.last_${randomPostFix}@mailinator.com"
        LOG.debug("Creating email for customer {}", email)
        customer = new Customer(email.toString(), "John", "Smith", "password12345", null, null, null, null)
    }

    def cleanup() {
        setup: "remove any registered accounts during spec run"
        List<RegisteredAccount> listOfAccounts = bulkAccountService.search(customer.getEmail())
        if (!listOfAccounts.isEmpty()) {
            for (RegisteredAccount account : listOfAccounts) {
                LOG.debug("Removing customer {} with email {}", account.getCustomerNumber(), account.getEmail())
                bulkAccountService.removeAccount(account.getCustomerNumber())
            }
        }
    }

    def "user is presented with an error when no email is entered"() {
        given: "Goto the Login Page"
        to LoginPage

        expect: "that we are at the Login Page"
        at LoginPage

        when: "I hit the delete key on the email address and enter a space for the password"
        with(loginModule) {
            emailAddress << Keys.chord(Keys.DELETE)
            password << " "
        }

        then: "I expect to see a dynamic error asking for my email address"
        loginModule.emailAddressError == "Please enter your email address"
    }

    def "user is presented with an error when invalid email is presented"() {

        given: "I am going to the Login Page"
        to LoginPage

        expect: "To goto the login page"
        at LoginPage

        when: "I fill in the email address with a invaild email address and attemt to log in"
        with(loginModule) {
            emailAddress << "abc.def"
            password << "passwors12345"
            login.click(LoginPage)
        }

        then: "expect to see a contextual error on the page stating that it failed"
        loginModule.getFormErrors()[0] == "Sorry, this does not match our records. Check your spelling and try again."
    }

    def "user is presented with an error when no password is entered"() {
        given: "go to login page"
        to LoginPage

        expect: "user is at login page"
        at LoginPage

        when: "enter blanks for login information"
        with(loginModule) {
            password << Keys.chord(Keys.DELETE)
            emailAddress << " "
        }

        then: "ensure error appears"
        loginModule.passwordError == "Please enter your password"
    }

    def "attempt to login with a failed login"() {
        given: "go to login page"
        to LoginPage

        expect: "user is at login page"
        at LoginPage

        when: "enter false login information"
        with(loginModule) {
            emailAddress << "iAmNotRegisteredAndNeverWillBe@example.com"
            password << "doesNotReallyMatter"
            login.click(LoginPage)
        }

        then: "ensure error appears"
        loginModule.getFormErrors()[0] == "Sorry, this does not match our records. Check your spelling and try again."
    }

    def "new user can sign up for a regular account"() {

        given: "goto the registration page"
        to RegisterPage

        expect: "user is at registration page"
        at RegisterPage

        when: "fill the registration form as a regular user"
        registerForm.firstName << customer.getFirstName()
        registerForm.lastName << customer.getLastName()
        registerForm.email = ""
        registerForm.email << customer.getEmail()
        registerForm.confirmEmail = ""
        registerForm.confirmEmail << customer.getEmail()
        registerForm.password << customer.getPassword()
        registerForm.confirmPassword << customer.getPassword()

        registerForm.addToEmailList.click()

        and: "submit the form"
        registerForm.submit.click()

        then: "make sure we are at the results page"
        at RegistrationResultPage

        and: "ensure user is logged in in the header"
        primaryHeader(customer.getFirstName().toString())

        when: "go to logout page"
        to LogoutPage

        then: "go to login page"
        to LoginPage
        waitFor { loginModule.present }

        when: "enter login information"
        loginModule.emailAddress << customer.getEmail()
        loginModule.password << customer.getPassword()
        loginModule.login.click(LoginPage)

        then: "user is at registration results page"
        at RegistrationResultPage

        when: "user goes to logout page"
        to LogoutPage

        then: "user is at logout page"
        at LoginPage
    }

}